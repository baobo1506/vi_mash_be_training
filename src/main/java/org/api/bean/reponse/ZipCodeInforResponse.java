package org.api.bean.reponse;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * [OVERVIEW] User Role Of Response
 *
 * @author: (TanDX)
 * @version: 1.0
 * @History
 * [NUMBER]  [VER]     [DATE]          [USER]             [CONTENT]
 * --------------------------------------------------------------------------
 * 001       1.0       2022/07/01      (TanDX)     	 Create new
*/
public class ZipCodeInforResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	// Address1
    @JsonProperty("address1")
    private String address1;

    // Address2
    @JsonProperty("address2")
    private String address2;
    
    // Address3
    @JsonProperty("address3")
    private String address3;
    
    // kana1
    @JsonProperty("kana1")
    private String kana1;
    
    // kana2
    @JsonProperty("kana2")
    private String kana2;
    
    // kana3
    @JsonProperty("kana3")
    private String kana3;
    
    // Prefcode
    @JsonProperty("prefcode")
    private String prefcode;
    
    // Zipcode
    @JsonProperty("zipcode")
    private String zipcode;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getKana1() {
		return kana1;
	}

	public void setKana1(String kana1) {
		this.kana1 = kana1;
	}

	public String getKana2() {
		return kana2;
	}

	public void setKana2(String kana2) {
		this.kana2 = kana2;
	}

	public String getKana3() {
		return kana3;
	}

	public void setKana3(String kana3) {
		this.kana3 = kana3;
	}

	public String getPrefcode() {
		return prefcode;
	}

	public void setPrefcode(String prefcode) {
		this.prefcode = prefcode;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
    
}
