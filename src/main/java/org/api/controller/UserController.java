package org.api.controller;

import org.api.annotation.LogExecutionTime;
import org.api.bean.ResultBean;
import org.api.services.UserService;
import org.api.utils.ApiValidateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * [OVERVIEW] UserController.
 *
 * @author: (TanDX)
 * @version: 1.0
 * @History
 * [NUMBER]  [VER]     [DATE]          [USER]             [CONTENT]
 * --------------------------------------------------------------------------
 * 001       1.0       2022/07/15      (TanDX)        Create new
*/

@LogExecutionTime
@RestController
public class UserController {
    /** The Constant log. */
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    /** The info service. */
    @Autowired
    private UserService userService;

    /**
     * Add the user info.
     *
     * @author (TanDX)
     * @param json
     * @return the response entity
     */
    @PostMapping(value = "/api/user", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> addUser(@RequestBody String json) throws Exception, ApiValidateException {
        ResultBean resultBean = null;
        resultBean = userService.createUser(json);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.CREATED);
    }

    /**
     * Gets the user info.
     *
     * @author (TanDX)
     * @param id
     * @return the user info
     */
    @GetMapping(value = "/api/user/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getUserById(@PathVariable Integer id) throws Exception {
        ResultBean resultBean = null;
        resultBean = userService.getUserById(id);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    /**
     * Update user info.
     *
     * @author (TanDX)
     * @param json
     * @return the response entity
     */
    @PutMapping(value = "/api/user", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> updateUser(@RequestBody String json) throws Exception, ApiValidateException {
        ResultBean resultBean = null;
        resultBean = userService.updateUser(json);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    /**
     * Gets the users.
     *
     * @author (TanDX)
     * @param column
     * @param sort
     * @param keyWord
     * @param page
     * @param size
     * @return the users
     */
    @GetMapping(value = "/api/user", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getUser(String sort, String column, String keyWord, Integer page, Integer size) throws Exception {
        ResultBean resultBean = null;
        resultBean = userService.getUsers(sort,column, keyWord, page, size);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
}
