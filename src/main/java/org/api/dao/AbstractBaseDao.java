/////////////////////////////////////////////////////////////////////////////
//
// © 2022 API. All right reserved.
//
/////////////////////////////////////////////////////////////////////////////

package org.api.dao;

import java.math.BigInteger;
import java.util.Map;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.MapUtils;
import org.api.bean.reponse.search.PageResponse;
import org.api.bean.request.PageRequest;

/**
 * Abstract Base Dao.
*
 * @author: (TanDX)
 * @version: 1.0
 * @param <T>
 * @改訂履歴
 * [番号]     [VER]     [修正日]        [更新者]          [更新内容]
 * --------------------------------------------------------------------------
 * 001        1.0      2022/07/01       (TanDX)           Create New
 *
 */
public abstract class AbstractBaseDao {

    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext()
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T extends PageResponse> T findAll(PageRequest searchDTO) {
        String hsql = searchDTO.getQuery().toString().trim();
        Query query = this.entityManager.createQuery(hsql);
        if (searchDTO.hasPaging()) {
            query.setFirstResult(searchDTO.getFirstResult()).setMaxResults(searchDTO.getNoRecordInPage());
        }
        fillParams(query, searchDTO.getSearchFields());
        T instance;
        try {
            instance = (T) searchDTO.getResponseClass().getConstructor().newInstance();
            instance.pageInfo(searchDTO.getCurrentPage(), searchDTO.getNoRecordInPage(), countTotalRecord(searchDTO));
            if (searchDTO.hasPaging() && instance.getTotalPage() > 0 && (instance.getCurrentPage() > instance.getTotalPage())) {
                searchDTO.currentPage(instance.getTotalPage());
                query.setFirstResult(searchDTO.getFirstResult()).setMaxResults(searchDTO.getNoRecordInPage());
                instance.pageInfo(searchDTO.getCurrentPage(), searchDTO.getNoRecordInPage(), countTotalRecord(searchDTO));
            }
            instance.rawResults(query.getResultList());
            return instance;
        } catch (Exception e) {
            return null;
        }
    }

    protected Long countTotalRecord(PageRequest searchDTO) {
        Query query = this.entityManager.createQuery(searchDTO.getCount().toString());
        fillParams(query, searchDTO.getSearchFields());
        Object singResult = query.getSingleResult();
        if (Objects.isNull(singResult)) {
            return (long) 0;
        }
        return ((Long) singResult);
    }

    protected Long countTotalRecordWithNativeQuery(PageRequest searchDTO) {
        Query query = this.entityManager.createNativeQuery(searchDTO.getCount().toString());
        fillParams(query, searchDTO.getSearchFields());
        Object singResult = query.getSingleResult();
        if (Objects.isNull(singResult)) {
            return (long) 0;
        }
        return ((BigInteger) singResult).longValue();
    }

    protected void fillParams(Query query, Map<String, Object> searchParams) {
        if (MapUtils.isEmpty(searchParams)) {
            return;
        }
        searchParams.forEach(query::setParameter);
    }
}
