/////////////////////////////////////////////////////////////////////////////
//
// © 2022 API. All right reserved.
//
/////////////////////////////////////////////////////////////////////////////

package org.api.dao.impl;

import org.api.bean.reponse.search.user.UserPageResponse;
import org.api.bean.request.UserSearchListRequest;
import org.api.dao.AbstractBaseDao;
import org.api.dao.user.UserLoginDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * [OVERVIEW] UserLoginDaoImpl.
 *
 * @author: (TanDX)
 * @version: 1.0
 * @History
 * [NUMBER]  [VER]     [DATE]          [USER]             [CONTENT]
 * --------------------------------------------------------------------------
 * 001       1.0       2022/07/01      (TanDX)         Create new
*/

@Repository
public class UserLoginDaoImpl extends AbstractBaseDao implements UserLoginDao {

    /** The Constant log. */
    private static final Logger log = LoggerFactory.getLogger(UserLoginDaoImpl.class);

    /**
     * Find add.
     *
     * @author (TanDX)
     * @param search
     * @return UserPageResponse
     */
    @Override
    public UserPageResponse getAll(UserSearchListRequest search) {
        return super.findAll(search);
    }
}
