/////////////////////////////////////////////////////////////////////////////
//
// © 2022 API. All right reserved.
//
/////////////////////////////////////////////////////////////////////////////

package org.api.dao.user;

import org.api.bean.reponse.search.user.UserPageResponse;
import org.api.bean.request.UserSearchListRequest;

/**
 * [OVERVIEW] UserLoginDao.
 *
 * @author:  (TanDX)
 * @version: 1.0
 * @History
 * [NUMBER]  [VER]     [DATE]          [USER]             [CONTENT]
 * --------------------------------------------------------------------------
 * 001       1.0       2022/07/01      (TanDX)      	 Create new
*/
public interface UserLoginDao {
    /**
     * Find add.
     *
     * @author (TanDX)
     * @param search the search
     * @return the user info page response
     */
    public UserPageResponse getAll(UserSearchListRequest search);
}
