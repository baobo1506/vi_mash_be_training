/////////////////////////////////////////////////////////////////////////////
//
// © 2022 API. All right reserved.
//
/////////////////////////////////////////////////////////////////////////////

package org.api.dao.user;

import java.util.Optional;

import org.api.bean.jpa.UserLoginEntity;
import org.api.dao.BaseRepository;

/**
 * [OVERVIEW] UserLoginDao.
 *
 * @author:  (TanDX)
 * @version: 1.0
 * @History
 * [NUMBER]  [VER]     [DATE]          [USER]             [CONTENT]
 * --------------------------------------------------------------------------
 * 001       1.0       2022/07/01      (TanDX)      	 Create new
*/
public interface UserLoginJPA extends BaseRepository<UserLoginEntity, Integer> {

    /**
     * Find user login
     * @author (TanDX)
     * @param userId
     * @return the user login entity
     */
    public Optional<UserLoginEntity> findOneById(Integer userId);


}
